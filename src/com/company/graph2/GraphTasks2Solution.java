package com.company.graph2;

import java.util.*;

public class GraphTasks2Solution implements GraphTasks2 {

    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {

        Set<Integer> hashSet = new HashSet<>();
        HashMap<Integer, Integer> answerMap = new HashMap<>();

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            answerMap.put(i, Integer.MAX_VALUE);
        }
        answerMap.put(startIndex, 0);
        hashSet.add(startIndex);

        while (hashSet.size() != adjacencyMatrix.length) {

            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (!hashSet.contains(i) && i != startIndex && adjacencyMatrix[startIndex][i] > 0) {
                    answerMap.put(i, Math.min(answerMap.get(i), answerMap.get(startIndex) + adjacencyMatrix[startIndex][i]));

                }
            }
            int min = Integer.MAX_VALUE;
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (answerMap.get(i) < min && i != startIndex && !hashSet.contains(i)) {
                    min = answerMap.get(i);
                    startIndex = i;
                }
            }
            hashSet.add(startIndex);
        }
        return answerMap;

    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        Set<Integer> isVisited = new TreeSet<>();
        int startVertax = (int)(Math.random()*adjacencyMatrix.length);
        isVisited.add(startVertax);
        int result = 0;
        while (isVisited.size() != adjacencyMatrix.length) {
            int firstVertax = 0;
            int secondVertax = 0;
            int min = Integer.MAX_VALUE;
            for (Integer i : isVisited) {
                for (int j = 0; j < adjacencyMatrix[i].length; j++) {
                    int weight = adjacencyMatrix[i][j];
                    if (weight < min && weight != 0 && (!isVisited.contains(j))) {
                        firstVertax = i;
                        secondVertax = j;
                        min = weight;
                    }
                }
            }
            isVisited.add(firstVertax);
            isVisited.add(secondVertax);
            result += min;
        }

        return result;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        List<Edge> edges = getEdgeList(adjacencyMatrix);

        Collections.sort(edges);

        int n = adjacencyMatrix.length;
        int result = 0;

        int[] sets = new int[n];
        for (int i = 0; i < n; i++) {
            sets[i] = i;
        }

        for (Edge e : edges) {
            if (isBridge(sets, e)){
                result += e.getWeight();
                combine(sets, e);
            }
        }

        return result;
    }

    private static List<Edge> getEdgeList(int[][] matrix) {
        ArrayList<Edge> edges = new ArrayList<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                int value = matrix[i][j];
                if (value != 0) {
                    int min = Math.min(i, j);
                    int max = Math.max(i, j);
                    Edge edge = new Edge(min, max, value);
                    if (!edges.contains(edge)) edges.add(new Edge(min, max, value));
                }
            }
        }
        return edges;
    }

    private static boolean isBridge(int[] sets, Edge e) {
        int u = e.getU();                       // [0, 1, 2, 3, 4, 5] 0, 1 лежат в разных компонентах связности
        int v = e.getV();                       // [0, 1, 0, 0, 0, 0]

        return sets[u] != sets[v];
    }

    private static void combine(int[] sets, Edge e) {
        int u = e.getU();
        int v = e.getV();
        int uSet = sets[u];
        int vSet = sets[v];
        int max = Math.max(uSet, vSet);
        int min = Math.min(uSet, vSet);

        for (int i = 0; i < sets.length; i++) {
            if (sets[i] == max) sets[i] = min;  // Не имеет значения к какой КС присоединять, присоед к меньшей
        }
    }

}
